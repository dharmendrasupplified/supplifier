<div class="inventor-statistics-total-post-views">
    <?php $total_views = Inventor_Statistics_Logic::listing_views_get_total( get_the_ID() ); ?>
    <i class="fa fa-eye"></i>
    <?php echo _n( sprintf( '<strong>%d</strong> view', $total_views ), sprintf( '<strong>%d</strong> views', $total_views), $total_views, 'inventor-statistics' ); ?>
</div>