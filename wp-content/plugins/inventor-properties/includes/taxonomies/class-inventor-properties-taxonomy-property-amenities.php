<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class Inventor_Properties_Taxonomy_Property_Amenities
 *
 * @class Inventor_Properties_Taxonomy_Property_Amenities
 * @package Inventor/Classes/Taxonomies
 * @author Pragmatic Mates
 */
class Inventor_Properties_Taxonomy_Property_Amenities {
    /**
     * Initialize taxonomy
     *
     * @access public
     * @return void
     */
    public static function init() {
        add_action( 'init', array( __CLASS__, 'definition' ) );
        add_action( 'parent_file', array( __CLASS__, 'menu' ) );
        add_action( 'admin_menu', array( __CLASS__, 'admin_menu' ) );
    }

    /**
     * Widget definition
     *
     * @access public
     * @return void
     */
    public static function definition() {
        $labels = array(
            'name'              => __( 'Property Amenities', 'inventor' ),
            'singular_name'     => __( 'Property Amenity', 'inventor' ),
            'search_items'      => __( 'Search Property Amenity', 'inventor' ),
            'all_items'         => __( 'All Property Amenities', 'inventor' ),
            'parent_item'       => __( 'Parent Property Amenity', 'inventor' ),
            'parent_item_colon' => __( 'Parent Property Amenity:', 'inventor' ),
            'edit_item'         => __( 'Edit Property Amenity', 'inventor' ),
            'update_item'       => __( 'Update Property Amenity', 'inventor' ),
            'add_new_item'      => __( 'Add New Property Amenity', 'inventor' ),
            'new_item_name'     => __( 'New Property Amenity', 'inventor' ),
            'menu_name'         => __( 'Property Amenities', 'inventor' ),
            'not_found'         => __( 'No property amenities found.', 'inventor' ),
        );

        register_taxonomy( 'property_amenities', array( 'property' ), array(
            'labels'            => $labels,
            'hierarchical'      => true,
            'query_var'         => 'property-amenity',
            'rewrite'           => array( 'slug' => __( 'property-amenity', 'inventor' ), 'hierarchical' => true ),
            'public'            => true,
            'show_ui'           => false,
            'show_admin_column' => false,
        ) );
    }

    /**
     * Set active menu for taxonomy amenity
     *
     * @access public
     * @return string
     */
    public static function menu( $parent_file ) {
        global $current_screen;
        $taxonomy = $current_screen->taxonomy;

        if ( 'property_amenities' == $taxonomy ) {
            return 'lexicon';
        }

        return $parent_file;
    }

    /**
     * Registers to Inventor admin menu
     *
     * @return void
     */
    public static function admin_menu() {
        add_submenu_page( 'lexicon', __( 'Property amenities', 'inventor' ), __( 'Properties: amenities', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=property_amenities', false );
    }
}

Inventor_Properties_Taxonomy_Property_Amenities::init();
