<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class Inventor_Properties_Taxonomy_Property_Types
 *
 * @class Inventor_Properties_Taxonomy_Property_Types
 * @package Inventor/Classes/Taxonomies
 * @author Pragmatic Mates
 */
class Inventor_Properties_Taxonomy_Property_Types {
    /**
     * Initialize taxonomy
     *
     * @access public
     * @return void
     */
    public static function init() {
        add_action( 'init', array( __CLASS__, 'definition' ) );
        add_action( 'parent_file', array( __CLASS__, 'menu' ) );
        add_action( 'admin_menu', array( __CLASS__, 'admin_menu' ) );
    }

    /**
     * Widget definition
     *
     * @access public
     * @return void
     */
    public static function definition() {
        $labels = array(
            'name'              => __( 'Property Types', 'inventor' ),
            'singular_name'     => __( 'Property Type', 'inventor' ),
            'search_items'      => __( 'Search Property Type', 'inventor' ),
            'all_items'         => __( 'All Property Types', 'inventor' ),
            'parent_item'       => __( 'Parent Property Type', 'inventor' ),
            'parent_item_colon' => __( 'Parent Property Type:', 'inventor' ),
            'edit_item'         => __( 'Edit Property Type', 'inventor' ),
            'update_item'       => __( 'Update Property Type', 'inventor' ),
            'add_new_item'      => __( 'Add New Property Type', 'inventor' ),
            'new_item_name'     => __( 'New Property Type', 'inventor' ),
            'menu_name'         => __( 'Property Types', 'inventor' ),
            'not_found'         => __( 'No property types found.', 'inventor' ),
        );

        register_taxonomy( 'property_types', array( 'property' ), array(
            'labels'            => $labels,
            'hierarchical'      => true,
            'query_var'         => 'property-type',
            'rewrite'           => array( 'slug' => __( 'property-type', 'inventor' ), 'hierarchical' => true ),
            'public'            => true,
            'show_ui'           => false,
            'show_admin_column' => true,
        ) );
    }

    /**
     * Set active menu for taxonomy property type
     *
     * @access public
     * @return string
     */
    public static function menu( $parent_file ) {
        global $current_screen;
        $taxonomy = $current_screen->taxonomy;

        if ( 'property_types' == $taxonomy ) {
            return 'lexicon';
        }

        return $parent_file;
    }

    /**
     * Registers to Inventor admin menu
     *
     * @return void
     */
    public static function admin_menu() {
        add_submenu_page( 'lexicon', __( 'Property types', 'inventor' ), __( 'Properties: types', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=property_types', false );
    }
}

Inventor_Properties_Taxonomy_Property_Types::init();
