Version: 0.1.0
Release date: 2015-11-13
What's new:
- First release


Version: 0.2.0
Release date: 2015-11-13
What's new:
- taxonomies moved from core inventor plugin
- new fields: reference, year built, garages, parking lots, home area, lot area, lot dimensions
- new groups: valuation, public facilities
- option to hide unassigned amenities


Version: 0.2.1
Release date: 2015-11-16
What's new:
- custom post type loading priority fix
- listing category metabox


Version: 0.2.2
Release date: 2015-11-19
What's new:
- deleted custom table columns to unify listing interface
- validation of positive number fields


Version: 0.3.0
Release date: 2015-11-26
What's new:
- support for inventor-shop (enables listing purchasing)


Version: 0.3.1
Release date: 2015-12-02
What's new:
- inventor_claims_allowed_listing_post_types filter