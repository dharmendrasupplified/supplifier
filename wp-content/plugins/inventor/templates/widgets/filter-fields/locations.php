<?php if ( empty( $instance['hide_location'] ) ) : ?>
	<div class="form-group">
		<?php if ( 'labels' == $input_titles ) : ?>
			<label for="<?php echo ! empty( $field_id_prefix ) ? $field_id_prefix : ''; ?><?php echo esc_attr( $args['widget_id'] ); ?>_locations"><?php echo __( 'Location', 'inventor' ); ?></label>
		<?php endif; ?>

        <?php $locations = get_terms( 'locations', array(
            'hide_empty'    => false,
        ) ); ?>

		<select class="form-control"
                name="filter-locations"
                data-size="10"
                <?php if ( count( $locations ) > 10 ) : ?>data-live-search="true"<?php endif;?>
                id="<?php echo ! empty( $field_id_prefix ) ? $field_id_prefix : ''; ?><?php echo esc_attr( $args['widget_id'] ); ?>_locations">
			<option value="">
				<?php if ( 'placeholders' == $input_titles ) : ?>
					<?php echo __( 'Location', 'inventor' ); ?>
				<?php else : ?>
					<?php echo __( 'All locations', 'inventor' ); ?>
				<?php endif; ?>
			</option>

            <?php $locations = get_terms( 'locations', array(
                'hide_empty' 	=> false,
                'parent'		=> 0,
            ) ); ?>

			<?php if ( is_array( $locations ) ) : ?>
				<?php foreach ( $locations as $location ) : ?>
					<option value="<?php echo esc_attr( $location->term_id ); ?>" <?php if ( ! empty( $_GET['filter-locations'] ) && $_GET['filter-locations'] == $location->term_id ) : ?>selected="selected"<?php endif; ?>><?php echo esc_html( $location->name ); ?></option>

					<?php $sublocations = get_terms( 'locations', array(
						'hide_empty'    => false,
						'parent'        => $location->term_id,
					) ); ?>

					<?php if ( is_array( $sublocations ) ) : ?>
						<?php foreach ( $sublocations as $sublocation ) : ?>
							<option value="<?php echo esc_attr( $sublocation->term_id ); ?>" <?php if ( ! empty( $_GET['filter-locations'] ) && $_GET['filter-locations'] == $sublocation->term_id ) : ?>selected="selected"<?php endif; ?>>
								&raquo;&nbsp; <?php echo esc_html( $sublocation->name ); ?>
							</option>

							<?php $subsublocations = get_terms( 'locations', array(
								'hide_empty' 	=> false,
								'parent' 		=> $sublocation->term_id,
							) ); ?>

							<?php if ( is_array( $subsublocations ) ) : ?>
								<?php foreach ( $subsublocations as $subsublocation ) : ?>
									<option value="<?php echo esc_attr( $subsublocation->term_id ); ?>" <?php if ( ! empty( $_GET['filter-locations'] ) && $_GET['filter-locations'] == $subsublocation->term_id ) : ?>selected="selected"<?php endif; ?>>
										&nbsp;&nbsp;&nbsp;&raquo;&nbsp; <?php echo esc_html( $subsublocation->name ); ?>
									</option>
								<?php endforeach; ?>
							<?php endif; ?>
						<?php endforeach; ?>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endif; ?>
		</select>
	</div><!-- /.form-group -->
<?php endif; ?>