<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
get_header(); ?>

<section id="primary" class="content-area">
	<main id="main" class="site-main content" role="main">
		<?php if ( have_posts() ) : ?>
			<header class="page-header">
				<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
				<?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
			</header><!-- .page-header -->

			<?php if ( get_theme_mod( 'inventor_general_show_listing_archive_as_grid', null ) == '1' ) : ?>
				<div class="listing-box-archive type-box items-per-row-4">
			<?php endif; ?>

			<div class="listings-row">

			<?php $index = 0; ?>
			<?php while ( have_posts() ) : the_post(); ?>
			<div class="listing-container">
				<?php if ( get_theme_mod( 'inventor_general_show_listing_archive_as_grid', null ) == '1' ) : ?>
					<?php echo Inventor_Template_Loader::load( 'listings/box' ); ?>
				<?php else : ?>
					<?php echo Inventor_Template_Loader::load( 'listings/row' ); ?>
				<?php endif; ?>

			</div><!-- /.listing-container -->

			<?php if ( 0 == ( ( $index + 1 ) % 4 ) && Inventor_Query::loop_has_next() ) : ?>
			</div><div class="listings-row">
		<?php endif; ?>

			<?php $index++; ?>

		<?php endwhile; ?>

			<?php if ( get_theme_mod( 'inventor_general_show_listing_archive_as_grid', null ) == '1' ) : ?>
				</div><!-- /.listings-row -->
				</div><!-- /.listing-box-archive -->
			<?php endif; ?>

			<?php the_posts_pagination( array(
				'prev_text'          => __( 'Previous', 'inventor' ),
				'next_text'          => __( 'Next', 'inventor' ),
				'mid_size'              => 2,
			) ); ?>
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

	</main><!-- .site-main -->
</section><!-- .content-area -->

<?php get_footer(); ?>