<?php if ( empty( $listing ) ): ?>
    <div class="alert alert-warning">
        <?php echo __( 'You have to specify listing!', 'inventor' ) ?>
    </div>
<?php else: ?>
    <h3><?php echo __( sprintf( "What's wrong with %s?", get_the_title( $listing ) ), 'inventor' ); ?></h3>

    <form method="post" action="<?php echo get_the_permalink( $listing ) ?>">
        <input type="hidden" name="listing_id" value="<?php echo $listing->ID; ?>">

        <div class="form-group">
            <input type="radio" class="form-control" name="reason" id="spam" required value="<?php echo __( 'It is spam', 'inventor' ); ?>">
            <label for="spam"><?php echo __( 'It is spam', 'inventor' ); ?></label>

            <input type="radio" class="form-control" name="reason" id="nudity" required value="<?php echo __( 'It is nudity or pornography', 'inventor' ); ?>">
            <label for="nudity"><?php echo __( 'It is nudity or pornography', 'inventor' ); ?></label>

            <input type="radio" class="form-control" name="reason" id="humiliation" required value="<?php echo __( 'It humiliates somebody', 'inventor' ); ?>">
            <label for="humiliation"><?php echo __( 'It humiliates somebody', 'inventor' ); ?></label>

            <input type="radio" class="form-control" name="reason" id="inappropriate" required value="<?php echo __( 'It is inappropriate', 'inventor' ); ?>">
            <label for="inappropriate"><?php echo __( 'It is inappropriate', 'inventor' ); ?></label>

            <input type="radio" class="form-control" name="reason" id="attack" required value="<?php echo __( 'It insults or attacks someone based on their religion, ethnicity or sexual orientation', 'inventor' ); ?>">
            <label for="attack"><?php echo __( 'It insults or attacks someone based on their religion, ethnicity or sexual orientation', 'inventor' ); ?></label>

            <input type="radio" class="form-control" name="reason" id="violence" required value="<?php echo __( 'It advocates violence or harm to a person or animal', 'inventor' ); ?>">
            <label for="violence"><?php echo __( 'It advocates violence or harm to a person or animal', 'inventor' ); ?></label>

            <input type="radio" class="form-control" name="reason" id="harming" required value="<?php echo __( 'It displays someone harming themself or planning to harm themself', 'inventor' ); ?>">
            <label for="harming"><?php echo __( 'It displays someone harming themself or planning to harm themself', 'inventor' ); ?></label>

            <input type="radio" class="form-control" name="reason" id="drugs" required value="<?php echo __( 'It describes buying or selling drugs, guns or regulated products', 'inventor' ); ?>">
            <label for="drugs"><?php echo __( 'It describes buying or selling drugs, guns or regulated products', 'inventor' ); ?></label>

            <input type="radio" class="form-control" name="reason" id="unauthorized" required value="<?php echo __( 'It is an unauthorized use of my intellectual property', 'inventor' ); ?>">
            <label for="unauthorized"><?php echo __( 'It is an unauthorized use of my intellectual property', 'inventor' ); ?></label>

            <input type="radio" class="form-control" name="reason" id="something_else" required value="<?php echo __( 'Something else', 'inventor' ); ?>">
            <label for="something_else"><?php echo __( 'Something else', 'inventor' ); ?></label>
        </div><!-- /.form-group -->

        <div class="form-group">
            <textarea class="form-control" name="message" placeholder="<?php echo __( 'Other reason or additional information', 'inventor' ); ?>" rows="4"></textarea>
        </div><!-- /.form-group -->

        <h3><?php echo __( 'Contact to you', 'inventor' ); ?></h3>

        <div class="row">
            <div class="form-group col-md-6">
                <input class="form-control" name="name" type="text" placeholder="<?php echo __( 'Name', 'inventor' ); ?>" required="required">
            </div><!-- /.form-group -->

            <div class="form-group col-md-6">
                <input class="form-control" name="email" type="email" placeholder="<?php echo __( 'E-mail', 'inventor' ); ?>" required="required">
            </div><!-- /.form-group -->
        </div><!-- /.row -->

        <?php if ( class_exists( 'Inventor_Recaptcha' ) ) : ?>
            <?php if ( Inventor_Recaptcha::is_recaptcha_enabled() ) : ?>
                <div id="recaptcha-<?php echo esc_attr( $this->id );  ?>"class="recaptcha" data-sitekey="<?php echo get_theme_mod( 'inventor_recaptcha_site_key' ); ?>"></div>
            <?php endif; ?>
        <?php endif; ?>

        <div class="button-wrapper">
            <button type="submit" class="btn btn-primary" name="report_form"><?php echo __( 'Submit to review', 'inventor' ); ?></button>
        </div><!-- /.button-wrapper -->
    </form>
<?php endif; ?>