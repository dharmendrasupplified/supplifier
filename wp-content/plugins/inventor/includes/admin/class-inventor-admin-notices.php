<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Inventor_Admin_Notices
 *
 * @class Inventor_Admin_Notices
 * @package Inventor/Classes/Admin
 * @author Pragmatic Mates
 */
class Inventor_Admin_Notices {
	/**
	 * Initialize
	 *
	 * @access public
	 * @return void
	 */
	public static function init() {
		add_action( 'admin_notices', array( __CLASS__, 'check_plugins' ) );
	}

	/**
	 * Count number of columns in plugins table
	 *
	 * @return int
	 */
	public static function plugins_table_cols_count() {
		$table = _get_list_table( 'WP_Plugins_List_Table' );
		return $table->get_column_count();
	}

	/**
	 * Checks if it needed to render plugin purchase code information
	 *
	 * @access public
	 * @return void
	 */
	public static function check_plugins() {
		$purchase_code = get_theme_mod( 'inventor_purchase_code', null );

		if ( empty( $purchase_code ) ) {
			$plugins = get_plugins();

			foreach ( $plugins as $key => $value ) {
				if ( substr( $key, 0, strlen( 'inventor' ) ) !== 'inventor' ) {
					continue;
				}

				add_action( 'after_plugin_row_' . $key, array( __CLASS__, 'render_purchase_code' ), 10, 3 );
			}
		}
	}

	/**
	 * Renders purchase code information
	 *
	 * @access public
	 * @return void
	 */
	public static function render_purchase_code() {
		echo Inventor_Template_Loader::load( 'admin/purchase-code', array(
			'count' => self::plugins_table_cols_count(),
		) );
	}
}

Inventor_Admin_Notices::init();