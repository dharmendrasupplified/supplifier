<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Inventor_Admin_Menu
 *
 * @class Inventor_Admin_Menu
 * @package Inventor/Classes/Admin
 * @author Pragmatic Mates
 */
class Inventor_Admin_Menu {
	/**
	 * Initialize
	 *
	 * @access public
	 * @return void
	 */
	public static function init() {
		add_action( 'custom_menu_order', '__return_true' );
		add_filter( 'menu_order', array( __CLASS__, 'menu_reorder' ) );

		add_action( 'admin_menu', array( __CLASS__, 'admin_menu' ) );
		add_action( 'admin_menu', array( __CLASS__, 'admin_separator' ) );
		add_action( 'admin_menu', array( __CLASS__, 'counter' ), 1 );
	}

	/**
	 * Reorder submenus
	 *
	 * @access public
	 * @param $menu_order
	 * @return mixed
	 */
	public static function menu_reorder( $menu_order ) {
		global $submenu;

		$menu_slugs = array( 'inventor', 'lexicon', 'listings');

		if ( ! empty( $submenu ) && ! empty( $menu_slugs ) && is_array( $menu_slugs ) ) {
			foreach( $menu_slugs as $slug ) {
				if ( ! empty( $submenu[ $slug ] ) ) {
					usort( $submenu[ $slug ], array( __CLASS__, 'sort_alphabet' ) );
				}
			}
		}

		return $menu_order;
	}

	/**
	 * Compare alphabetically
	 *
	 * @access public
	 * @param $a
	 * @param $b
	 * @return int
	 */
	public static function sort_alphabet( $a, $b ) {
		return strnatcmp( $a[0], $b[0] );
	}

	/**
	 * Count post types under listings menu item
	 */
	public static function counter() {
		global $wp_post_types;

		foreach ( $wp_post_types as $post_type ) {
			if ( 'listings' === $post_type->show_in_menu ) {
				$name = $post_type->name;
				$published = wp_count_posts( $name )->publish;
				$draft = wp_count_posts( $name )->draft;
				$pending = wp_count_posts( $name )->pending;
				$count = $published + $draft + $pending;
				$name_with_count = sprintf('%s <span class="inventor-menu-count">(%s)</span>', $wp_post_types[ $name ]->labels->all_items, $count );
				$wp_post_types[ $name ]->labels->all_items =  $name_with_count;
			}
		}
	}

	/**
	 * Registers separator
	 *
	 * @return void
	 */
	public static function admin_separator() {
		global $menu;

		$menu[49] = array( '', 'read', 'separator', '', 'wp-menu-separator' );
	}

	/**
	 * Registers Inventor admin menu wrapper
	 *
	 * @return void
	 */
	public static function admin_menu() {
		add_menu_page( __( 'Inventor', 'inventor' ), __( 'Inventor', 'inventor' ), 'edit_posts', 'inventor', null, null, '50' );
        add_submenu_page( 'inventor', __( 'Colors', 'inventor' ), __( 'Colors', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=colors', false );
		add_submenu_page( 'inventor', __( 'Locations', 'inventor' ), __( 'Locations', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=locations', false );
		add_submenu_page( 'inventor', __( 'Categories', 'inventor' ), __( 'Categories', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=listing_categories', false );
		remove_submenu_page( 'inventor', 'inventor' );

		add_menu_page( __( 'Listings', 'inventor' ), __( 'Listings', 'inventor' ), 'edit_posts', 'listings', null, null, '51' );
		add_menu_page( __( 'Lexicon', 'inventor' ), __( 'Lexicon', 'inventor' ), 'edit_posts', 'lexicon', null, null, '52' );
        add_submenu_page( 'lexicon', __( 'Car models', 'inventor' ), __( 'Cars: models', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=car_models', false );
        add_submenu_page( 'lexicon', __( 'Car body styles', 'inventor' ), __( 'Cars: body styles', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=car_body_styles', false );
        add_submenu_page( 'lexicon', __( 'Car engine types', 'inventor' ), __( 'Cars: engine types', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=car_engine_types', false );
        add_submenu_page( 'lexicon', __( 'Car transmissions', 'inventor' ), __( 'Cars: transmissions', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=car_transmissions', false );
        add_submenu_page( 'lexicon', __( 'Dating groups', 'inventor' ), __( 'Dating: groups', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=dating_groups', false );
        add_submenu_page( 'lexicon', __( 'Dating interests', 'inventor' ), __( 'Dating: interests', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=dating_interests', false );
        add_submenu_page( 'lexicon', __( 'Dating statuses', 'inventor' ), __( 'Dating: statuses', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=dating_statuses', false );
        add_submenu_page( 'lexicon', __( 'Education subjects', 'inventor' ), __( 'Education: subjects', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=education_subjects', false );
        add_submenu_page( 'lexicon', __( 'Education levels', 'inventor' ), __( 'Education: levels', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=education_levels', false );
        add_submenu_page( 'lexicon', __( 'Event types', 'inventor' ), __( 'Events: types', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=event_types', false );
        add_submenu_page( 'lexicon', __( 'Food kinds', 'inventor' ), __( 'Food & Drinks: kinds', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=food_kinds', false );
        add_submenu_page( 'lexicon', __( 'Hotel classes', 'inventor' ), __( 'Hotels: classes', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=hotel_classes', false );
        add_submenu_page( 'lexicon', __( 'Job positions', 'inventor' ), __( 'Jobs: positions', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=job_positions', false );
        add_submenu_page( 'lexicon', __( 'Job skills', 'inventor' ), __( 'Jobs: skills', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=job_skills', false );
        add_submenu_page( 'lexicon', __( 'Pet animals', 'inventor' ), __( 'Pets: animals', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=pet_animals', false );
        add_submenu_page( 'lexicon', __( 'Shopping categories', 'inventor' ), __( 'Shopping: categories', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=shopping_categories', false );
        add_submenu_page( 'lexicon', __( 'Travel activities', 'inventor' ), __( 'Travel: activities', 'inventor' ), 'edit_posts', 'edit-tags.php?taxonomy=travel_activities', false );
        remove_submenu_page( 'lexicon', 'lexicon' );
	}
}

Inventor_Admin_Menu::init();