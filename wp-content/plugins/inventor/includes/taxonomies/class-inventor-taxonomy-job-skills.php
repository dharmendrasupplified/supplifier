<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class Inventor_Taxonomy_Job_Skills
 *
 * @class Inventor_Taxonomy_Job_Skills
 * @package Inventor/Classes/Taxonomies
 * @author Pragmatic Mates
 */
class Inventor_Taxonomy_Job_Skills {
    /**
     * Initialize taxonomy
     *
     * @access public
     * @return void
     */
    public static function init() {
        add_action( 'init', array( __CLASS__, 'definition' ) );
        add_action( 'parent_file', array( __CLASS__, 'menu' ) );
    }

    /**
     * Widget definition
     *
     * @access public
     * @return void
     */
    public static function definition() {
        $labels = array(
            'name'              => __( 'Job Skills', 'inventor' ),
            'singular_name'     => __( 'Job Skill', 'inventor' ),
            'search_items'      => __( 'Search Job Skill', 'inventor' ),
            'all_items'         => __( 'All Job Skills', 'inventor' ),
            'parent_item'       => __( 'Parent Job Skill', 'inventor' ),
            'parent_item_colon' => __( 'Parent Job Skill:', 'inventor' ),
            'edit_item'         => __( 'Edit Job Skill', 'inventor' ),
            'update_item'       => __( 'Update Job Skill', 'inventor' ),
            'add_new_item'      => __( 'Add New Job Skill', 'inventor' ),
            'new_item_name'     => __( 'New Job Skill', 'inventor' ),
            'menu_name'         => __( 'Job Skills', 'inventor' ),
            'not_found'         => __( 'No job skills found.', 'inventor' ),
        );

        register_taxonomy( 'job_skills', array( 'job' ), array(
            'labels'            => $labels,
            'hierarchical'      => true,
            'query_var'         => 'job-skill',
            'rewrite'           => array( 'slug' => __( 'job-skill', 'inventor' ), 'hierarchical' => true ),
            'public'            => true,
            'show_ui'           => true,
            'show_in_menu'      => false,
            'show_in_nav_menus' => false,
            'meta_box_cb'       => false,
            'show_admin_column' => true,
        ) );
    }

    /**
     * Set active menu for taxonomy job skill
     *
     * @access public
     * @return string
     */
    public static function menu( $parent_file ) {
        global $current_screen;
        $taxonomy = $current_screen->taxonomy;

        if ( 'job_skills' == $taxonomy ) {
            return 'lexicon';
        }

        return $parent_file;
    }
}

Inventor_Taxonomy_Job_Skills::init();
