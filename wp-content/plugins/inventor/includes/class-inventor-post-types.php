<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Inventor_Post_Types
 *
 * @class Inventor_Post_Types
 * @package Inventor/Classes/Post_Types
 * @author Pragmatic Mates
 */
class Inventor_Post_Types {
	public static $listings_types = array();

	/**
	 * Initialize listing types
	 *
	 * @access public
	 * @return void
	 */
	public static function init() {
		self::includes();

		add_action( 'init', array(__CLASS__, 'set_all_listing_post_types' ), 12 );
		add_action( 'init', array(__CLASS__, 'disable_post_types' ), 12 );
	}

	/**
	 * Loads listing types
	 *
	 * @access public
	 * @return void
	 */
	public static function includes() {
		require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-transaction.php';
		require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-report.php';
		require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-user.php';

		require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-listing.php';
		require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-business.php';
		require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-car.php';
		require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-dating.php';
        require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-education.php';
		require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-event.php';
        require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-food.php';
        require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-hotel.php';
		require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-job.php';
		require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-pet.php';
		require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-shopping.php';
		require_once INVENTOR_DIR . 'includes/post-types/class-inventor-post-type-travel.php';
	}

	/**
	 * Remove post types base on theme settings
	 */
	public static function disable_post_types() {
		global $wp_post_types;

		$post_types_all = self::get_listing_post_types();
		$post_types_supported = get_theme_mod( 'inventor_general_post_types', $post_types_all );
		$post_types_unsupported = array_diff( $post_types_all, $post_types_supported );

		if ( is_array( $post_types_unsupported ) ) {
			foreach( $post_types_unsupported as $post_type ) {
				if ( ! empty( $wp_post_types[ $post_type ] ) ) {
					unset( $wp_post_types[ $post_type ] );
				}
			}
		}
	}

	/**
	 * Get list of available post types
	 *
	 * @access public
	 * @param bool $include_abstract
	 * @return array
	 */
	public static function get_listing_post_types( $include_abstract = false ) {
		$listings_types = array();

		$post_types = get_post_types( array(), 'objects' );

		if ( ! empty( $post_types ) ) {
			foreach ( $post_types as $post_type ) {
				if ( $post_type->show_in_menu === 'listings' ) {
					$listings_types[] = $post_type->name;
				}
			}
		}

		// Sort alphabetically
		sort( $listings_types );

		if ( $include_abstract ) {
			array_unshift( $listings_types, 'listing' );
		}

		return $listings_types;
	}

	/**
	 * Gets all enabled/disabled listing post types
	 *
	 * @access public
	 * @return array
	 */
	public static function get_all_listing_post_types() {
		return self::$listings_types;
	}


	/**
	 * Set listing post types before we unregister some of them
	 *
	 * @access public
	 * @return array
	 */
	public static function set_all_listing_post_types() {
		$post_types = get_post_types( array(), 'objects' );
		if ( ! empty( $post_types ) ) {
			foreach ( $post_types as $post_type ) {
				if ( $post_type->show_in_menu === 'listings' ) {
					self::$listings_types[] = get_post_type_object( $post_type->name );
				}
			}
		}

		return self::$listings_types;
	}

	/**
	 * Get listing by its id
	 *
	 * @access public
	 * @param int $id
	 * @return object
	 */
	public static function get_listing( $id ) {
		$post = get_post( $id );

		if ( empty( $post ) || ! in_array( $post->post_type, self::get_listing_post_types() ) ) {
			return null;
		}

		return $post;
	}

	/**
	 * Returns count of all listings of specified post type and status
	 *
	 * @access public
	 * @param $post_types array
	 * @return int
	 */
	public static function count_post_types( $post_types, $status = 'publish' ) {
		$result = 0;

		if ( ! is_array( $post_types ) ) {
			$post_types = array( $post_types );
		}

		foreach ( $post_types as $post_type ) {
			if ( ! empty( wp_count_posts( $post_type )->$status ) ) {
				$result += wp_count_posts( $post_type )->$status;
			}
		}

		return $result;
	}

	/**
	 * Adds metabox to post type
	 *
	 * @access public
	 * @param $post_type
	 * @param array $metaboxes
	 * @return void
	 */
	public static function add_metabox( $post_type, array $metaboxes ) {
		if ( sizeof( $metaboxes ) > 0 ) {
			foreach ( $metaboxes as $metabox ) {
				if ( 2 === count( explode( '::', $metabox ) ) ) {
					$parts = explode( '::', $metabox );
					$name = 'metabox_' . $parts[1];
					$parts[0]::$name( $post_type );
				} else {
					$name = 'metabox_' . $metabox;
					Inventor_Metaboxes::$name( $post_type );
				}
			}
		}
	}

	/**
	 * Removes metabox from post type
	 *
	 * @access public
	 * @param $post_type
	 * @param array $metaboxes
	 * @return void
	 */
	public static function remove_metabox( $post_type, array $metaboxes ) {
		if ( sizeof( $metaboxes ) > 0 ) {
			foreach ( $metaboxes as $metabox ) {
				$metabox_id = INVENTOR_LISTING_PREFIX . $post_type . '_' . $metabox;

				if ( ! empty( $metabox_id ) ) {
					CMB2_Boxes::remove( $metabox_id );
				}
			}
		}
	}

	/**
	 * Returns current status of opening hours
	 *
	 * @access public
	 * @param $listing_id int
	 * @return string
	 */
	public static function opening_hours_status( $listing_id, $day = null ) {
		// @TODO: Use timezone field
		
		$opening_hours = get_post_meta( $listing_id, INVENTOR_LISTING_PREFIX . 'opening_hours', true );

		if ( empty( $opening_hours ) ) {
			return 'unknown';
		}

		$week = array( 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY' );

		$today_index = date( 'N' );
		$week_day = $week[ $today_index - 1 ];

		if( $day != null && $day != $week_day ) {
			return 'other-day';
		}

		// find opening hours for today
		foreach( $opening_hours as $opening_day ) {
			if( $opening_day['listing_day'] == $week_day ) {
				$time_from = strtotime( $opening_day['listing_time_from'] );
				$time_to = strtotime( $opening_day['listing_time_to'] );
				$now = time();

				return $time_from <= $now && $now <= $time_to ? 'open' : 'closed';
			}
		}

		return 'closed';
	}

	/**
	 * Check if listing has opening hours
	 *
	 * @access public
	 * @param $listing_id
	 * @return bool
	 */
	public static function opening_hours_visible( $listing_id = null ) {
		if ( $listing_id == null ) {
			$listing_id = get_the_ID();
		}

		$opening_hours = get_post_meta( $listing_id, INVENTOR_LISTING_PREFIX . 'opening_hours', true );

		if ( is_array( $opening_hours ) ) {
			foreach( $opening_hours as $opening_hour ) {
				if ( ! empty( $opening_hour['listing_time_from'] ) || ! empty( $opening_hour['listing_time_to'] ) || ! empty( $opening_hour['listing_custom'] ) ) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Gets list of attributes for post
	 *
	 * @param null $post_id
	 * @return array
	 */
	public static function get_attributes( $post_id = null ) {
		$results = array();

		if ( empty( $post_id ) ) {
			$post_id = get_the_ID();
		}

		$post_type = get_post_type( $post_id );
		$meta_boxes = CMB2_Boxes::get_all();

		// Price
		$price = Inventor_Price::get_price( $post_id );
		if ( ! empty( $price ) ) {
			$results['price'] = array(
				'name'      => __( 'Price', 'inventor' ),
				'value'     => $price
			);
		}

		foreach ( $meta_boxes as $meta_box ) {
			$object_types = $meta_box->meta_box['object_types'];

			if ( ! empty( $object_types ) && ! in_array( 'listing', $object_types ) && ! in_array( $post_type, $object_types ) ) {
				continue;
			}

			if ( ! empty( $meta_box->meta_box['skip'] ) && true == $meta_box->meta_box['skip'] ) {
				continue;
			}

			$fields = $meta_box->meta_box['fields'];
			foreach ( $fields as $field ) {
				$value = get_post_meta( $post_id, $field['id'], true );

				if ( ! empty( $field['skip'] ) && true == $field['skip'] ) {
					continue;
				}

				// Select
				if ( in_array( $field['type'], array( 'select', 'radio', 'radio_inline' ) ) ) {
					$value = empty( $field['options'][$value] ) ? $value : $field['options'][$value];
				}

				// Regular taxonomies
				if ( in_array( $field['type'], array( 'taxonomy_select', 'taxonomy_radio_inline', 'taxonomy_multicheck_inline', 'taxonomy_multicheck_hierarchy') ) ) {
					$terms = wp_get_post_terms( $post_id, $field['taxonomy'] );
					$count = count( $terms );

					if ( is_array( $terms ) && $count > 0 ) {
						$value = '';
						$index = 0;

						foreach ( $terms as $term ) {

							$value .= $term->name;
							if ( $index + 1 != $count ) {
								$value .= ', ';
							}
							$index++;
						}
					}
				}

				if ( empty( $value ) ) {
					continue;
				}

				// Taxonomy multicheck hierarchy for locations
				if ( 'taxonomy_multicheck_hierarchy' == $field['type'] ) {
					if ( 'locations' == $field['taxonomy'] ) {
						$value = Inventor_Query::get_listing_location_name( $post_id );
					}
				}

				// Email
				if ( 'text_email' == $field['type'] ) {
					$value = '<a href="mailto:' . $value . '">' . $value .'</a>';
				}

				// URL
				if ( 'text_url' == $field['type'] ) {
					$value = '<a href="' . $value . '">' . str_replace( array( 'http://', 'https://' ), '', $value ) .'</a>';
				}

				// Money
				if ( 'text_money' == $field['type'] ) {
					$value = Inventor_Price::format_price( $value );
				}

				// Checkbox
				if ( 'checkbox' == $field['type'] ) {
					if ( 'on' == $value ) {
						$value = __( 'Yes', 'inventor' );
					} else {
						$value = __( 'No', 'inventor' );
					}
				}

				// Text Date Timestamp
				if ( 'text_date_timestamp' == $field['type'] ) {
					$value = date_i18n( get_option( 'date_format' ), $value );
				}

				// ColorPicker
				if ( 'colorpicker' == $field['type'] ) {
					$value = sprintf( '<span class="listing-color" style="background-color: %s"></span>', $value );
				}

				// Weight
				if ( INVENTOR_LISTING_PREFIX  . 'weight' == $field['id'] ) {
					$weight_unit = get_theme_mod( 'inventor_measurement_weight_unit', 'lbs' );
					$value = sprintf( '%s %s', $value, $weight_unit );
				}

				// Apply filter
				$value = apply_filters( 'inventor_attribute_value', $value, $field );

				// Automatically skip attributes without name (groups)
				if ( ! empty( $field['name'] ) ) {
					$results[ $field['id'] ] = array(
						'name'  => $field['name'],
						'value' => $value,
					);
				}
			}
		}

		return $results;
	}
}

Inventor_Post_Types::init();
