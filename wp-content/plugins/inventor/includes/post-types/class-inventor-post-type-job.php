<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Inventor_Post_Type_Job
 *
 * @class Inventor_Post_Type_Job
 * @package Inventor/Classes/Post_Types
 * @author Pragmatic Mates
 */
class Inventor_Post_Type_Job {
	/**
	 * Initialize custom post type
	 *
	 * @access public
	 * @return void
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'definition' ) );
        add_action( 'cmb2_init', array( __CLASS__, 'fields' ) );
        add_filter( 'inventor_claims_allowed_listing_post_types', array( __CLASS__, 'allowed_claiming' ) );
    }

    /**
     * Defines if post type can be claimed
     *
     * @access public
     * @param array $post_types
     * @return array
     */
    public static function allowed_claiming( $post_types ) {
        $post_types[] = 'job';
        return $post_types;
    }

	/**
	 * Custom post type definition
	 *
	 * @access public
	 * @return void
	 */
	public static function definition() {
		$labels = array(
			'name'                  => __( 'Jobs', 'inventor' ),
			'singular_name'         => __( 'Job', 'inventor' ),
			'add_new'               => __( 'Add New Job', 'inventor' ),
			'add_new_item'          => __( 'Add New Job', 'inventor' ),
			'edit_item'             => __( 'Edit Job', 'inventor' ),
			'new_item'              => __( 'New Job', 'inventor' ),
			'all_items'             => __( 'Jobs', 'inventor' ),
			'view_item'             => __( 'View Job', 'inventor' ),
			'search_items'          => __( 'Search Job', 'inventor' ),
			'not_found'             => __( 'No Jobs found', 'inventor' ),
			'not_found_in_trash'    => __( 'No Jobs Found in Trash', 'inventor' ),
			'parent_item_colon'     => '',
			'menu_name'             => __( 'Jobs', 'inventor' ),
		);

		register_post_type( 'job',
			array(
				'labels'            => $labels,
				'show_in_menu'	  => 'listings',
				'supports'          => array( 'title', 'editor', 'thumbnail', 'comments', 'author' ),
				'has_archive'       => true,
				'rewrite'           => array( 'slug' => __( 'jobs', 'inventor' ) ),
				'public'            => true,
				'show_ui'           => true,
				'categories'        => array(),
			)
		);
	}

    /**
     * Defines custom fields
     *
     * @access public
     * @return array
     */
    public static function fields() {
        Inventor_Post_Types::add_metabox( 'job', array( 'general' ) );

        $cmb = new_cmb2_box( array(
            'id'            => INVENTOR_LISTING_PREFIX . 'job_details',
            'title'         => __( 'Details', 'inventor' ),
            'object_types'  => array( 'job' ),
            'context'       => 'normal',
            'priority'      => 'high',
        ) );

        $cmb->add_field( array(
            'name'              => __( 'Employer', 'inventor' ),
            'id'                => INVENTOR_LISTING_PREFIX  . 'job_employer',
            'type'              => 'text',
            'default'           => Inventor_Submission::get_submission_field_value( INVENTOR_LISTING_PREFIX . 'job_details', INVENTOR_LISTING_PREFIX  . 'job_employer' ),
        ) );

        $cmb->add_field( array(
            'name'              => __( 'Starting date', 'inventor' ),
            'id'                => INVENTOR_LISTING_PREFIX  . 'job_starting_date',
            'type'              => 'text_date_timestamp',
            'default'           => Inventor_Submission::get_submission_field_value( INVENTOR_LISTING_PREFIX . 'job_details', INVENTOR_LISTING_PREFIX  . 'job_starting_date' ),
        ) );

        $cmb->add_field( array(
            'name'              => __( 'Job position', 'inventor' ),
            'id'                => INVENTOR_LISTING_PREFIX  . 'job_position',
            'type'              => 'taxonomy_multicheck_hierarchy',
            'taxonomy'          => 'job_positions',
            'default'           => Inventor_Submission::get_submission_field_value( INVENTOR_LISTING_PREFIX . 'job_details', INVENTOR_LISTING_PREFIX  . 'job_position' ),
        ) );

        $cmb->add_field( array(
            'name'              => __( 'Contract type', 'inventor' ),
            'id'                => INVENTOR_LISTING_PREFIX  . 'job_contract_type',
            'type'              => 'radio',
            'options'           => array(
                'FULL_TIME'              => __( 'Full-time', 'inventor' ),
                'PART_TIME'              => __( 'Part-time', 'inventor' ),
                'BRIGADE'                => __( 'Brigade', 'inventor' ),
                'AGREEMENT'              => __( 'Agreement', 'inventor' ),
                'SELF_EMPLOYED_PERSON'   => __( 'Self-employed persons', 'inventor' ),
            ),
            'default'           => Inventor_Submission::get_submission_field_value( INVENTOR_LISTING_PREFIX . 'job_details', INVENTOR_LISTING_PREFIX  . 'job_contract_type' ),
        ) );

        $cmb->add_field( array(
            'name'              => __( 'Required skills', 'inventor' ),
            'id'                => INVENTOR_LISTING_PREFIX  . 'job_skill',
            'type'              => 'taxonomy_multicheck_hierarchy',
            'taxonomy'          => 'job_skills',
            'default'           => Inventor_Submission::get_submission_field_value( INVENTOR_LISTING_PREFIX . 'job_details', INVENTOR_LISTING_PREFIX  . 'job_skill' ),
        ) );

        Inventor_Post_Types::add_metabox( 'job', array( 'banner', 'price', 'location', 'flags', 'contact', 'social', 'listing_category' ) );
    }
}

Inventor_Post_Type_Job::init();