<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Inventor_Customizations_General
 *
 * @class Inventor_Customizations_General
 * @package Inventor/Classes/Customizations
 * @author Pragmatic Mates
 */
class Inventor_Customizations_General {
	/**
	 * Initialize customization type
	 *
	 * @access public
	 * @return void
	 */
	public static function init() {
		add_action( 'customize_register', array( __CLASS__, 'customizations' ) );
	}

	/**
	 * Customizations
	 *
	 * @access public
	 * @param object $wp_customize
	 * @return void
	 */
	public static function customizations( $wp_customize ) {
		$pages = Inventor_Utilities::get_pages();

		$wp_customize->add_section( 'inventor_general', array(
			'title' 	=> __( 'Inventor General', 'inventor' ),
			'priority' 	=> 1,
		) );	

		// Post Types
		$post_types = Inventor_Post_Types::get_all_listing_post_types();
		$types = array();

		if ( is_array( $post_types ) ) {
			foreach ( $post_types as $obj ) {
				$types[ $obj->name ] = $obj->labels->name;
			}
		}

		$wp_customize->add_setting( 'inventor_general_post_types', array(
			'default'           => $types,
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => array( 'Inventor_Customize_Control_Checkbox_Multiple', 'sanitize' ),
		) );

		$wp_customize->add_control( new Inventor_Customize_Control_Checkbox_Multiple(
			$wp_customize,
			'inventor_general_post_types',
			array(
				'section'       => 'inventor_general',
				'label'         => __( 'Post types', 'inventor' ),
				'choices'       => $types,
				'description'   => __( 'After changing post types make sure that your resave permalinks in "Settings - Permalinks"', 'inventor' ),
			)
		) );

		// Log in after registration
		$wp_customize->add_setting( 'inventor_log_in_after_registration', array(
			'default'           => null,
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		) );

		$wp_customize->add_control( 'inventor_log_in_after_registration', array(
			'type'          => 'checkbox',
			'label'         => __( 'Automatically log user in after registration', 'inventor' ),
			'section'       => 'inventor_general',
			'settings'      => 'inventor_log_in_after_registration',
		) );

		// Show listing archive as grid
		$wp_customize->add_setting( 'inventor_general_show_listing_archive_as_grid', array(
			'default'           => false,
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		) );

		$wp_customize->add_control( 'inventor_general_show_listing_archive_as_grid', array(
			'type'      => 'checkbox',
			'label'     => __( 'Show listing archive as grid', 'inventor' ),
			'section'   => 'inventor_general',
			'settings'  => 'inventor_general_show_listing_archive_as_grid',
		) );

		// Purchase code
		$wp_customize->add_setting( 'inventor_purchase_code', array(
			'default'           => null,
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		) );

		$wp_customize->add_control( 'inventor_purchase_code', array(
			'type'          => 'text',
			'label'         => __( 'Purchase code', 'inventor' ),
			'section'       => 'inventor_general',
			'settings'      => 'inventor_purchase_code',
		) );
	}
}

Inventor_Customizations_General::init();
