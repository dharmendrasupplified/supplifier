Version: 0.1.0
Release date: 2015-11-03
What's new:
- First release


Version: 0.1.1
Release date: 2015-11-12
What's new:
- show_in_menu fix
- deleted custom admin table columns
- plugin URI


Version: 0.1.2
Release date: 2015-11-19
What's new:
- fixed cmb2 hook


Version: 0.1.3
Release date: 2015-11-26
What's new:
- missing image field fix
- changed coupon listing prefix
- create metaboxes file

Version: 0.1.4
Release date: 2015-11-28
What's new:
- notice fix
