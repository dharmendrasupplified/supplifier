Version: 0.1.0
Release date: 2015-11-03
What's new:
- First release


Version: 0.1.1
Release date: 2015-11-13
What's new:
- show_in_menu fix
- plugin URI


Version: 0.1.2
Release date: 2015-11-26
What's new:
- added items column in the invoice list table
- page setting moved to Inventor Pages section in WP customize


Version: 0.2.0
Release date: 2015-12-02
What's new:
- billing details retrieved from payment form instead of user profile
