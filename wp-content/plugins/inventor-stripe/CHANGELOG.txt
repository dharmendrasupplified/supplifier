Version: 0.1.0
Release date: 2015-11-03
What's new:
- First release


Version: 0.2.0
Release date: 2015-11-26
What's new:
- refactored business logic
- using inventor_prepare_payment filter
- listing purchase support
- inventor_submission_transactions_page renamed to inventor_submission_transactions_page


Version: 0.3.0
Release date: 2015-12-02
What's new:
- support for billing details within payment form
