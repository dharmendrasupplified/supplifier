<?php if ( empty( $instance['hide_rating'] ) ) : ?>
    <div class="form-group">
        <?php if ( 'labels' == $input_titles ) : ?>
            <label for="<?php echo ! empty( $field_id_prefix ) ? $field_id_prefix : ''; ?><?php echo esc_attr( $args['widget_id'] ); ?>_rating"><?php echo __( 'Rating', 'inventor' ); ?></label>
        <?php endif; ?>

        <select class="form-control"
                name="filter-rating"
                id="<?php echo ! empty( $field_id_prefix ) ? $field_id_prefix : ''; ?><?php echo esc_attr( $args['widget_id'] ); ?>_rating">
            <option value="">
                <?php if ( 'placeholders' == $input_titles ) : ?>
                    <?php echo __( 'Rating', 'inventor' ); ?>
                <?php else : ?>
                    <?php echo __( 'Any rating', 'inventor' ); ?>
                <?php endif; ?>
            </option>

            <option value="1" <?php if ( ! empty( $_GET['filter-rating'] ) && $_GET['filter-rating'] == 1 ) : ?>selected="selected"<?php endif; ?>>1+</option>
            <option value="2" <?php if ( ! empty( $_GET['filter-rating'] ) && $_GET['filter-rating'] == 2 ) : ?>selected="selected"<?php endif; ?>>2+</option>
            <option value="3" <?php if ( ! empty( $_GET['filter-rating'] ) && $_GET['filter-rating'] == 3 ) : ?>selected="selected"<?php endif; ?>>3+</option>
            <option value="4" <?php if ( ! empty( $_GET['filter-rating'] ) && $_GET['filter-rating'] == 4 ) : ?>selected="selected"<?php endif; ?>>4+</option>
            <option value="5" <?php if ( ! empty( $_GET['filter-rating'] ) && $_GET['filter-rating'] == 5 ) : ?>selected="selected"<?php endif; ?>>5</option>
        </select>
    </div><!-- /.form-group -->
<?php endif; ?>