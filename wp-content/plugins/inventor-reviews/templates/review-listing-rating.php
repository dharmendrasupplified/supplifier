<?php $rating = Inventor_Reviews_Logic::get_post_total_rating( $listing_id ); ?>

<?php if ( $display == null ): ?>
    <div class="inventor-reviews-rating <?php echo ( empty( $rating ) ) ? 'not-rated' : ''; ?>">
        <span class="review-rating-total" data-fontawesome data-path="<?php echo plugins_url(); ?>/inventor/libraries/raty/images" data-score="<?php echo esc_attr( $rating ); ?>" data-starOn="fa fa-star" data-starHalf="fa fa-star-half-o" data-starOff="fa fa-star-o"></span>
    </div><!-- /.inventor-reviews-rating -->
<?php endif; ?>

<?php if ( $display == 'row' ): ?>
    <dt><?php echo __( 'Rating', 'inventor-reviews' ); ?></dt>
    <dd class="<?php echo ( empty( $rating ) ) ? 'not-rated' : ''; ?>">
        <span class="review-rating" data-fontawesome data-path="<?php echo plugins_url(); ?>/inventor/libraries/raty/images" data-score="<?php echo esc_attr( $rating ); ?>" data-starOn="fa fa-star" data-starHalf="fa fa-star-half-o" data-starOff="fa fa-star-o"></span>
    </dd>
<?php endif; ?>

<?php if ( $display == 'box' ): ?>
    <div class="listing-box-rating <?php echo ( empty( $rating ) ) ? 'not-rated' : ''; ?>">
        <span class="review-rating" data-fontawesome data-path="<?php echo plugins_url(); ?>/inventor/libraries/raty/images" data-score="<?php echo esc_attr( $rating ); ?>" data-starOn="fa fa-star" data-starHalf="fa fa-star-half-o" data-starOff="fa fa-star-o"></span>
    </div><!-- /.listing-box-rating -->
<?php endif; ?>

<?php if ( $display == 'masonry' ): ?>
    <div class="listing-masonry-rating <?php echo ( empty( $rating ) ) ? 'not-rated' : ''; ?>">
        <span class="review-rating" data-fontawesome data-path="<?php echo plugins_url(); ?>/inventor/libraries/raty/images" data-score="<?php echo esc_attr( $rating ); ?>" data-starOn="fa fa-star" data-starHalf="fa fa-star-half-o" data-starOff="fa fa-star-o"></span>
    </div><!-- /.listing-masonry-rating -->
<?php endif; ?>
