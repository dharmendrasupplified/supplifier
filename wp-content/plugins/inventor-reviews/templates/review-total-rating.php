<div class="inventor-reviews-total-rating">
    <?php $total_rating = Inventor_Reviews_Logic::get_post_total_rating( get_the_ID() ); ?>
    <?php $reviews_count = get_comment_count( get_the_ID() ); ?>
    <i class="fa fa-star"></i> <strong><?php echo $total_rating ?> / 5 </strong>from <a href="#listing-detail-section-reviews"><?php echo $reviews_count['approved'] ?> <?php echo _n( 'review', 'reviews', $reviews_count['approved'], 'inventor-reviews' ); ?></a>
</div>