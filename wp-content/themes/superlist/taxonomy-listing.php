<?php
/**
 * The template for displaying taxonomy listing
 *
 * @package Superlist
 * @since Superlist 1.0.0
 */

get_header(); ?>
<?php $display_as_grid = ( ! empty( $_GET['listing-display'] ) && 'grid' == $_GET['listing-display'] ) ? true : get_theme_mod( 'inventor_general_show_listing_archive_as_grid', null ); ?>

	<section id="primary" class="content-area">
		<?php get_template_part( 'templates/document-title' ); ?>

		<?php dynamic_sidebar( 'content-top' ); ?>

		<main id="main" class="site-main content" role="main">
			<?php if ( have_posts() ) : ?>

				<?php if ( get_theme_mod( 'inventor_general_show_listing_archive_as_grid', null ) == '1' || $display_as_grid) : ?>
					<div class="listing-box-archive type-box items-per-row-4">
				<?php endif; ?>

				<div class="listings-row">

				<?php $index = 0; ?>
				<?php while ( have_posts() ) : the_post(); ?>
				<div class="listing-container">

					<?php if ( get_theme_mod( 'inventor_general_show_listing_archive_as_grid', null ) == '1'|| $display_as_grid ) : ?>
						<?php include Inventor_Template_Loader::locate( 'listings/box' ); ?>
					<?php else : ?>
						<?php include Inventor_Template_Loader::locate( 'listings/row' ); ?>
					<?php endif; ?>

				</div><!-- /.listing-container -->

				<?php if ( 0 == ( ( $index + 1 ) % 4 ) && Inventor_Query::loop_has_next() ) : ?>
				</div><div class="listings-row">
			<?php endif; ?>

				<?php $index++; ?>

			<?php endwhile; ?>

				<?php if ( get_theme_mod( 'inventor_general_show_listing_archive_as_grid', null ) == '1' || $display_as_grid ) : ?>
					</div><!-- /.listings-row -->
					</div><!-- /.listing-box-archive -->
				<?php endif; ?>

				<?php superlist_pagination(); ?>
			<?php else : ?>
				<?php get_template_part( 'templates/content', 'none' ); ?>
			<?php endif; ?>

			<?php dynamic_sidebar( 'content-bottom' ); ?>
		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php get_footer(); ?>
