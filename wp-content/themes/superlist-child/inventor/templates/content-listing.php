<div class="listing-detail">

    <!-- Gallery -->
    <?php do_action( 'inventor_before_listing_detail_gallery' ); ?>

    <?php if ( apply_filters( 'inventor_submission_listing_metabox_allowed', true, 'gallery', get_the_author_meta('ID') ) ): ?>
        <?php $gallery = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'gallery', true ); ?>
        <?php if ( ! empty( $gallery ) && is_array( $gallery ) ) : ?>
            <div class="listing-detail-section" id="listing-detail-section-gallery">
                <h2 class="page-header"><?php echo __( 'Gallery', 'inventor' ); ?></h2>

                <div class="listing-detail-gallery-wrapper">
                    <div class="listing-detail-gallery">
                        <?php $index = 0; ?>
                        <?php foreach ( $gallery as $id => $src ) : ?>
                            <a href="<?php echo esc_url( $src ); ?>" rel="listing-gallery" data-item-id="<?php echo esc_attr( $index++ ); ?>">
                                <div class="item-image" data-background-image="<?php echo esc_url( $src ); ?>"></div>
                            </a>
                        <?php endforeach; ?>
                    </div><!-- /.listing-detail-gallery -->

                    <div class="listing-detail-gallery-preview" data-count="<?php echo count( $gallery ) ?>">
                        <?php $index = 0; ?>
                        <?php foreach ( $gallery as $id => $src ) : ?>
                            <div data-item-id="<?php echo esc_attr( $index++ ); ?>">
                                <?php $img = wp_get_attachment_image_src( $id, 'thumbnail' ); ?>
                                <?php $img_src = $img[0]; ?>
                                <img src="<?php echo $img_src; ?>" alt="">
                            </div>
                        <?php endforeach; ?>
                    </div><!-- /.listing-detail-gallery-preview -->
                </div><!-- /.listing-detail-gallery-wrapper -->
            </div><!-- /.listing-detail-section -->
        <?php endif; ?>
    <?php endif; ?>

    <?php do_action( 'inventor_after_listing_detail_gallery' ); ?>

    <!-- Description -->
    <?php do_action( 'inventor_before_listing_detail_description' ); ?>

    <div class="listing-detail-section" id="listing-detail-section-description">
        <h2 class="page-header"><?php echo esc_attr__( 'Description', 'inventor' ); ?></h2>
        <?php the_content(); ?>
    </div><!-- /.listing-detail-section -->

    <?php do_action( 'inventor_after_listing_detail_description' ); ?>

    <!-- Attributes -->
    <?php do_action( 'inventor_before_listing_detail_attributes' ); ?>

    <?php $attributes = Inventor_Post_Types::get_attributes(); ?>
    <?php if ( ! empty( $attributes ) && is_array( $attributes ) && count( $attributes ) > 0 ) : ?>
        <div class="listing-detail-section" id="listing-detail-section-attributes">
            <h2 class="page-header"><?php echo __( 'Attributes', 'inventor' ); ?></h2>

            <div class="listing-detail-attributes">
                <ul>
                    <?php foreach( $attributes as $key => $attribute ) : ?>
                        <li class="<?php echo esc_attr( $key ); ?>">
                            <strong class="key"><?php echo wp_kses( $attribute['name'], wp_kses_allowed_html( 'post' ) ); ?></strong>
                            <span class="value"><?php echo wp_kses( $attribute['value'], wp_kses_allowed_html( 'post' ) ); ?></span>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div><!-- /.listing-detail-attributes -->
        </div><!-- /.listing-detail-section -->
    <?php endif; ?>

    <?php do_action( 'inventor_after_listing_detail_attributes' ); ?>

    <!-- Video -->
    <?php do_action( 'inventor_before_listing_detail_video' ); ?>

    <?php if ( apply_filters( 'inventor_submission_listing_metabox_allowed', true, 'video', get_the_author_meta('ID') ) ): ?>
        <?php $video = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'video', true ); ?>
        <?php if ( ! empty( $video ) ) : ?>
            <div class="listing-detail-section"  id="listing-detail-section-video">
                <h2 class="page-header"><?php echo __( 'Video', 'inventor' ); ?></h2>
                <div class="video-embed-wrapper">
                    <?php echo apply_filters( 'the_content', '[embed width="1280" height="720"]' . esc_attr( $video ) . '[/embed]' ); ?>
                </div><!-- /.video-embed-wrapper -->
            </div><!-- /.listing-detail-section -->
        <?php endif; ?>
    <?php endif; ?>

    <?php do_action( 'inventor_after_listing_detail_video' ); ?>

    <!-- Food menu -->
    <?php do_action( 'inventor_before_listing_detail_food_menu' ); ?>

    <?php $meals = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'food_menu_group', true ); ?>
    <?php if ( ! empty( $meals ) ) : ?>
        <div class="listing-detail-section" id="listing-detail-section-meals-and-drinks">
            <h2 class="page-header"><?php echo esc_attr__( 'Meals And Drinks', 'inventor' ); ?></h2>

            <div class="listing-detail-food-wrapper">
                <?php $groups = Inventor_Post_Type_Food::get_menu_groups(); ?>

                <?php if ( is_array( $groups['daily_menu'] ) && count( $groups['daily_menu'] ) > 0 ) : ?>
                    <h3><span><?php echo esc_attr__( 'Daily Menu', 'inventor' ); ?></span></h3>

                    <div class="listing-detail-food-inner">
                        <?php foreach( $groups['daily_menu'] as $meal ) : ?>
                            <?php echo Inventor_Template_Loader::load( 'post-types/food/menu-item', array(
                                'meal' => $meal,
                            ) ); ?>
                        <?php endforeach ?>
                    </div><!-- /.listing-detail-food-inner -->
                <?php endif; ?>

                <?php if ( is_array( $groups['menu'] ) && count( $groups['menu'] ) > 0 ) : ?>
                    <h3><span><?php echo esc_attr__( 'Menu', 'inventor' ); ?></span></h3>
                    <div class="listing-detail-food-inner">
                        <?php foreach( $groups['menu'] as $meal ) : ?>
                            <?php echo Inventor_Template_Loader::load( 'post-types/food/menu-item', array(
                                'meal' => $meal,
                            ) ); ?>
                        <?php endforeach ?>
                    </div><!-- /.listing-detail-food-inner -->
                <?php endif; ?>
            </div><!-- /.listing-detail-food-wrapper  -->
        </div><!-- /.listing-detail-section -->
    <?php endif; ?>

    <?php do_action( 'inventor_after_listing_detail_food_menu' ); ?>

    <!-- Opening hours -->
    <?php do_action( 'inventor_before_listing_detail_opening_hours' ); ?>

    <?php if ( apply_filters( 'inventor_submission_listing_metabox_allowed', true, 'opening_hours', get_the_author_meta('ID') ) ): ?>
        <?php $opening_hours = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'opening_hours', true ); ?>
        <?php $visible = Inventor_Post_Types::opening_hours_visible(); ?>

        <?php if ( ! empty( $opening_hours ) && $visible ) : ?>
            <div class="listing-detail-section" id="listing-detail-section-opening-hours">
                <h2 class="page-header"><?php echo __( 'Opening Hours', 'inventor' ); ?></h2>

                <table class="opening-hours horizontal">
                    <thead>
                        <tr>
                            <?php foreach( $opening_hours as $day ): ?>
                                <th>
                                    <?php echo $day['listing_day']; ?>
                                </th>
                            <?php endforeach; ?>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <?php foreach( $opening_hours as $day ): ?>
                                <td class="<?php echo Inventor_Post_Types::opening_hours_status( get_the_ID(), $day['listing_day'] ); ?>">
                                    <?php $time_from = empty ( $day['listing_time_from'] ) ? '' : $day['listing_time_from']; ?>
                                    <?php $time_to = empty ( $day['listing_time_to'] ) ? '' : $day['listing_time_to']; ?>
                                    <?php $custom_text = empty ( $day['listing_custom'] ) ? '' : $day['listing_custom']; ?>

                                    <?php if ( ! empty( $custom_text ) ) : ?>
                                        <?php $opening = $custom_text; ?>
                                    <?php else : ?>
                                        <?php $opening = "<span class=\"from\">{$time_from}</span> <span class=\"separator\">-</span> <span class=\"to\">{$time_to}</span>"; ?>
                                    <?php endif; ?>

                                    <?php $trim_characters = $time_from == '' && $time_to == '' ? ' -' : ' ' ?>
                                    <?php echo trim( $opening, $trim_characters ); ?>
                                </td>
                            <?php endforeach; ?>
                        </tr>
                    </tbody>
                </table>
            </div><!-- /.listing-detail-section -->
	    <?php endif; ?>
	<?php endif; ?>

    <?php do_action( 'inventor_after_listing_detail_opening_hours' ); ?>

    <!-- Google map -->
    <?php do_action( 'inventor_before_listing_detail_google_map' ); ?>

    <?php if ( apply_filters( 'inventor_submission_listing_metabox_allowed', true, 'location', get_the_author_meta('ID') ) ): ?>
        <?php $map_latitude = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'map_location_latitude', true );?>
        <?php $map_longitude = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'map_location_longitude', true );?>
        <?php $map_polygon = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'map_location_polygon', true );?>
        <?php $map = ! empty( $map_latitude ) || ! empty ( $map_longitude ); ?>

        <?php $street_view = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'street_view', true );?>
        <?php $inside_view = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'inside_view', true );?>

        <?php if ( class_exists( 'Inventor_Google_Map' ) && ( ! empty ( $map ) || ! empty ( $street_view ) ) ) : ?>
            <div class="listing-detail-section"  id="listing-detail-section-location">
                <h2 class="page-header"><?php echo __( 'Location', 'inventor' ); ?></h2>

                <div class="listing-detail-location-wrapper">
                    <!-- Nav tabs -->
                    <ul id="listing-detail-location" class="nav nav-tabs" role="tablist">

                        <?php if ( ! empty( $map ) ) : ?>
                            <li role="presentation" class="active">
                                <a href="#simple-map-panel" aria-controls="simple-map-panel" role="tab" data-toggle="tab">
                                    <i class="fa fa-map"></i><?php echo __( 'Map', 'inventor' ); ?>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if ( ! empty( $street_view ) ) : ?>
                            <li role="presentation" <?php echo empty( $map ) ? 'class="active"' : ''; ?>>
                                <a href="#street-view-panel" aria-controls="street-view-panel" role="tab" data-toggle="tab">
                                    <i class="fa fa-street-view"></i><?php echo __( 'Street View', 'inventor' ); ?>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if ( ! empty( $inside_view ) ) : ?>
                            <li role="presentation" <?php echo ( empty( $map ) && empty( $street_view ) ) ? 'class="active"' : ''; ?>>
                                <a href="#inside-view-panel" aria-controls="inside-view-panel" role="tab" data-toggle="tab">
                                    <i class="fa fa-home"></i><?php echo __( 'See Inside', 'inventor' ); ?>
                                </a>
                            </li>
                        <?php endif; ?>

                        <li class="directions">
                            <a href="https://maps.google.com?daddr=<?php echo esc_attr( $map_latitude ); ?>,<?php echo esc_attr( $map_longitude ); ?>">
                                <i class="fa fa-level-down"></i><?php echo __( 'Directions', 'inventor' ) ?>
                            </a>
                        </li>

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <?php if ( ! empty( $map ) ) : ?>
                            <div role="tabpanel" class="tab-pane fade in active" id="simple-map-panel">
                                <div class="detail-map">
                                    <div class="map-position">
                                        <div id="listing-detail-map"
                                             data-transparent-marker-image="<?php echo get_template_directory_uri(); ?>/assets/img/transparent-marker-image.png"
                                             data-latitude="<?php echo esc_attr( $map_latitude ); ?>"
                                             data-longitude="<?php echo esc_attr( $map_longitude ); ?>"
                                             data-polygon-path="<?php echo esc_attr( $map_polygon ); ?>"
                                             data-marker-style="simple">
                                        </div><!-- /#map-property -->
                                    </div><!-- /.map-property -->
                                </div><!-- /.detail-map -->
                            </div>
                        <?php endif; ?>

                        <?php if ( ! empty( $street_view ) ) : ?>
                            <?php $street_view_latitude = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'street_view_location_latitude', true );?>
                            <?php $street_view_longitude = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'street_view_location_longitude', true );?>
                            <?php $street_view_heading = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'street_view_location_heading', true );?>
                            <?php $street_view_pitch = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'street_view_location_pitch', true );?>
                            <?php $street_view_zoom = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'street_view_location_zoom', true );?>

                            <div role="tabpanel" class="tab-pane fade<?php echo empty( $map ) ? ' in active' : ''; ?>" id="street-view-panel">
                                <div id="listing-detail-street-view"
                                     data-latitude="<?php echo esc_attr( $street_view_latitude ); ?>"
                                     data-longitude="<?php echo esc_attr( $street_view_longitude ); ?>"
                                     data-heading="<?php echo esc_attr( $street_view_heading ); ?>"
                                     data-pitch="<?php echo esc_attr( $street_view_pitch ); ?>"
                                     data-zoom="<?php echo esc_attr( $street_view_zoom ); ?>">
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ( ! empty( $inside_view ) ) : ?>
                            <?php $inside_view_latitude = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'inside_view_location_latitude', true );?>
                            <?php $inside_view_longitude = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'inside_view_location_longitude', true );?>
                            <?php $inside_view_heading = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'inside_view_location_heading', true );?>
                            <?php $inside_view_pitch = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'inside_view_location_pitch', true );?>
                            <?php $inside_view_zoom = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'inside_view_location_zoom', true );?>

                            <div role="tabpanel" class="tab-pane fade<?php echo ( empty( $map ) && empty( $street_view ) ) ? ' in active' : ''; ?>" id="inside-view-panel">
                                <div id="listing-detail-inside-view"
                                     data-latitude="<?php echo esc_attr( $inside_view_latitude ); ?>"
                                     data-longitude="<?php echo esc_attr( $inside_view_longitude ); ?>"
                                     data-heading="<?php echo esc_attr( $inside_view_heading ); ?>"
                                     data-pitch="<?php echo esc_attr( $inside_view_pitch ); ?>"
                                     data-zoom="<?php echo esc_attr( $inside_view_zoom ); ?>">
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div><!-- /.listing-detail-section -->
        <?php endif; ?>
    <?php endif; ?>

    <?php do_action( 'inventor_after_listing_detail_google_map' ); ?>

    <!-- Contact -->
    <?php do_action( 'inventor_before_listing_detail_contact' ); ?>

    <?php if ( apply_filters( 'inventor_submission_listing_metabox_allowed', true, 'contact', get_the_author_meta('ID') ) ): ?>
        <?php $email = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'email', true ); ?>
        <?php $website = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'website', true ); ?>
        <?php $phone = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'phone', true ); ?>
        <?php $address = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'address', true ); ?>

        <?php if ( ! empty( $email ) || ! empty( $website ) || ! empty( $phone ) || ! empty( $address ) ) : ?>
            <div class="listing-detail-section" id="listing-detail-section-contact">
                <h2 class="page-header"><?php echo __( 'Contact', 'inventor' ); ?></h2>

                <div class="listing-detail-contact">
                    <div class="row">
                        <div class="col-md-6">
                            <ul>
                                <?php if ( ! empty( $email ) ): ?>
                                    <li class="email">
                                        <strong class="key"><?php echo __( 'E-mail', 'inventor' ); ?></strong>
                                        <span class="value">
                                            <a href="mailto:<?php echo esc_attr( $email ); ?>"><?php echo esc_attr( $email ); ?></a>
                                        </span>
                                    </li>
                                <?php endif; ?>
                                <?php if ( ! empty( $website ) ): ?>
                                    <li class="website">
                                        <strong class="key"><?php echo __( 'Website', 'inventor' ); ?></strong>
                                        <span class="value">
                                            <a href="<?php echo esc_attr( $website ); ?>"><?php echo esc_attr( $website ); ?></a>
                                        </span>
                                    </li>
                                <?php endif; ?>
                                <?php if ( ! empty( $phone ) ): ?>
                                    <li class="phone">
                                        <strong class="key"><?php echo __( 'Phone', 'inventor' ); ?></strong>
                                        <span class="value"><?php echo wp_kses( $phone, wp_kses_allowed_html( 'post' ) ); ?></span>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div><!-- /.col-* -->
                        <div class="col-md-6">
                            <ul>
                                <?php if ( ! empty( $address ) ): ?>
                                    <li class="address">
                                        <strong class="key"><?php echo __( 'Address', 'inventor' ); ?></strong>
                                        <span class="value"><?php echo wp_kses( nl2br( $address ), wp_kses_allowed_html( 'post' ) ); ?></span>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->
                </div><!-- /.listing-detail-contact -->
            </div><!-- /.listing-detail-section -->
        <?php endif; ?>
    <?php endif; ?>

    <?php do_action( 'inventor_after_listing_detail_contact' ); ?>

    <!-- Social -->
    <?php do_action( 'inventor_before_listing_detail_social' ); ?>

    <?php $social_metabox = CMB2_Boxes::get( INVENTOR_LISTING_PREFIX . get_post_type() . '_social' ); ?>
    <?php $has_social = false; ?>
    <?php if ( ! empty( $social_metabox ) ) : ?>
        <?php if ( is_array( $social_metabox->meta_box['fields'] ) ) : ?>
            <?php foreach ( $social_metabox->meta_box['fields'] as $key => $value ) : ?>
                <?php $social_value = get_post_meta( get_the_ID(), $value['id'], true ); ?>

                <?php if ( ! empty( $social_value ) ) : ?>
                    <?php $has_social = true; ?>
                <?php endif; ?>
            <?php endforeach; ?>

            <?php if ( $has_social ) : ?>
                <div id="listing-detail-section-social" class="listing-detail-section">
                    <h2 class="page-header"><?php echo __( 'Social Connections', 'inventor' ); ?></h2>
                    <?php foreach ( $social_metabox->meta_box['fields'] as $key => $value ) : ?>
                        <?php $social = get_post_meta( get_the_ID(), $value['id'], true ); ?>

                        <?php if ( ! empty( $social ) ) : ?>
                            <?php $parts =  explode( '_', $value['id'] ); ?>
                            <a href="<?php echo esc_attr( $social ); ?>"><i class="fa fa-<?php echo $parts[1]?>"></i></a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div><!-- /.listing-detail-section -->
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>

    <?php do_action( 'inventor_after_listing_detail_social' ); ?>

    <!-- FAQs -->
    <?php do_action( 'inventor_before_listing_detail_faq' ); ?>

    <?php $faqs = get_post_meta( get_the_ID(), INVENTOR_LISTING_PREFIX . 'faq', true );?>

    <?php if ( ! empty( $faqs ) && is_array( $faqs ) ) : ?>
        <div class="listing-detail-section" id="listing-detail-section-faq">
            <h2 class="page-header"><?php echo __( 'FAQ', 'inventor' ); ?></h2>

            <dl class="listing-detail-section-faq-list">
                <?php foreach ( $faqs as $faq ) : ?>
                    <dt><?php echo esc_attr( $faq[ INVENTOR_LISTING_PREFIX . 'question' ] ); ?></dt>
                    <dd><?php echo esc_attr( $faq[ INVENTOR_LISTING_PREFIX . 'answer' ] ); ?></dd>
                <?php endforeach; ?>
            </dl>
        </div><!-- /.listing-detail-section -->
    <?php endif; ?>

    <?php do_action( 'inventor_after_listing_detail_faq' ); ?>

    <!-- Comments / Reviews -->
    <?php do_action( 'inventor_before_listing_detail_comments' ); ?>

    <?php if ( apply_filters( 'inventor_submission_listing_metabox_allowed', true, 'reviews', get_the_author_meta('ID') ) ): ?>

        <?php if ( comments_open() || get_comments_number() ): ?>
            <?php comments_template( '', true ); ?>
        <?php endif; ?>

    <?php endif; ?>

    <?php do_action( 'inventor_after_listing_detail_comments' ); ?>

    <!-- Report -->
    <?php do_action( 'inventor_before_listing_detail_report' ); ?>

	<?php $reports_config = Inventor_Reports::get_config(); ?>
	<?php $report_page = $reports_config['page']; ?>
	<?php if ( ! empty( $report_page ) ): ?>
        <div class="listing-report">
            <a href="<?php echo get_permalink( $report_page ); ?>?id=<?php the_ID(); ?>" class="listing-report-btn">
                <i class="fa fa-flag-o"></i>
                <span><?php echo __( 'Report spam, abuse, or inappropriate content', 'inventor' ); ?></span>
            </a><!-- /.listing-report-btn-->
        </div><!-- /.listing-report -->
	<?php endif; ?>

    <?php do_action( 'inventor_after_listing_detail_report' ); ?>

</div><!-- /.listing-detail -->
