<?php
/**
 * Superlist child functions and definitions
 *
 * @package Superlist Child
 * @since Superlist Child 1.0.0
 */
 

add_action( 'wp_ajax_nopriv_sendsms', 'sendsms' );
add_action( 'wp_ajax_sendsms', 'sendsms' );
add_action( 'wp_ajax_nopriv_verifyotp', 'verifyotp' );
add_action( 'wp_ajax_verifyotp', 'verifyotp' );
function sendsms() {
	$number=trim($_POST['number']);
	 if ( username_exists( $number ) ) {
		echo 0; 
	 }
	 else {
	$digits = 4;
	$otp=rand(pow(10, $digits-1), pow(10, $digits)-1);
	$_SESSION['user_otp']=$otp;
	$_SESSION['user_otp'];
	$smstxt=urlencode("Dear user $otp is your OTP. Happy Supplifying!");
	$smstxt = str_replace("%21", "!", $smstxt);
	$url ="http://23.254.128.22:9080/urldreamclient/dreamurl?userName=sup_tech&password=supTech123&clientid=supTedst29&to=".$number."&text=".$smstxt."&Senderid=SUPTRX";	
	$ch = curl_init($url);    // initialize curl handle
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);
	echo 1;
	wp_die();
	}
}

function verifyotp() {
	$otp=trim($_POST['otpnumber']);
	if($_SESSION['user_otp']==$otp) :
		echo 1;
		unset($_SESSION['user_otp']);
		else :
			echo 0;
	endif;
	wp_die();
}

add_image_size('featured-size', 263, 200 );
add_image_size('custom-1', 750, 300 );
add_image_size('custom-2', 360, 630 );
add_image_size('custom-3', 360, 300 );

function my_js_variables(){ ?>
      <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url( "admin-ajax.php" ); ?>';
        //var ajaxnonce = '<?php echo wp_create_nonce( "itr_ajax_nonce" ); ?>';
      </script><?php
}
add_action ( 'wp_head', 'my_js_variables' );