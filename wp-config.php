<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'supplifier');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
 define('WP_MEMORY_LIMIT', '64M');
define('AUTH_KEY',         'D8xb>>^wl$3f:n9?AIAtm*SCs.{o_?VvjC-<V:p|9:KVGW+SP3]=n}Z{;7>XOr?v');
define('SECURE_AUTH_KEY',  'j3`l6$?&FVQ$`4;o-U`61:rA5Qc9q<H``6**cW3BhjUMcPt,OcoD[HutR}R4oR>c');
define('LOGGED_IN_KEY',    'u}<]1mDEauJigX}C,2H!W%Zfgxdo|me&kb=g?|U`^C{6qN2H-w.6@|;#R(P3=2J-');
define('NONCE_KEY',        '[xTxn|4%_T];]dBb UUXc2z*1aQL#!WBy|,9@[3i_q2AWRcH&E/1cdxfg18QS.sb');
define('AUTH_SALT',        'raV;bqk4i$J`!W/S?+oB(i30aRMi1%sP.7TgThz=3p$xeS|Tc%Hns4c(K`{ELe/l');
define('SECURE_AUTH_SALT', 'MM.>(E#7W)lc`frX=0p&MB6XER0*Ba&U5[j^{thXF{XL+0uFAP%osZ%w1wG:L1_|');
define('LOGGED_IN_SALT',   'KH7TaG@SJUsZjEZ^BGL>yz:}tP>S9sTJiL%+^|Kehb|z!Fjhj_Kt(X72`A Y0l5^');
define('NONCE_SALT',       'pQX7B/4_{d$vs12Y,~?OXF-bMjjBEJ~:E4&sA1}Yvg^T:Vk%~[yU0dP(@d>@KKhG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'su_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
 define('WP_DEBUG', false);
//@ini_set('display_errors', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
